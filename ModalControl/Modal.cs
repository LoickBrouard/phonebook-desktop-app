﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace ModalControl
{
    /// <summary>
    /// Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.
    ///
    /// Step 1a) Using this custom control in a XAML file that exists in the current project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:ModalControl"
    ///
    ///
    /// Step 1b) Using this custom control in a XAML file that exists in a different project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:ModalControl;assembly=ModalControl"
    ///
    /// You will also need to add a project reference from the project where the XAML file lives
    /// to this project and Rebuild to avoid compilation errors:
    ///
    ///     Right click on the target project in the Solution Explorer and
    ///     "Add Reference"->"Projects"->[Select this project]
    ///
    ///
    /// Step 2)
    /// Go ahead and use your control in the XAML file.
    ///
    ///     <MyNamespace:CustomControl1/>
    ///
    /// </summary>
    public class Modal : ContentControl
    {

        public bool IsOpen
        {
            get { return (bool)GetValue(IsOpenProperty); }
            set { SetValue(IsOpenProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsOpen.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsOpenProperty =
            DependencyProperty.Register("IsOpen", typeof(bool), typeof(Modal), new PropertyMetadata(false));



        public string ModalTitle
        {
            get { return (string)GetValue(ModalTitleProperty); }
            set { SetValue(ModalTitleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ModalTitle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ModalTitleProperty =
            DependencyProperty.Register("ModalTitle", typeof(string), typeof(Modal), new PropertyMetadata("Alert"));




        public ICommand ModalCommand
        {
            get { return (ICommand)GetValue(ModalCommandProperty); }
            set { SetValue(ModalCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ModalCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ModalCommandProperty =
            DependencyProperty.Register("ModalCommand", typeof(ICommand), typeof(Modal), new PropertyMetadata(null));

   
        static Modal()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Modal), new FrameworkPropertyMetadata(typeof(Modal)));
            BackgroundProperty.OverrideMetadata(typeof(Modal), new FrameworkPropertyMetadata(CreateDefaultBackground()));
        }

        private static object CreateDefaultBackground()
        {
            return new SolidColorBrush(Colors.Black)
            {
                Opacity = 0.5
            };
        }
    }
}
