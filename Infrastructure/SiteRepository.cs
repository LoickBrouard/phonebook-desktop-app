﻿
using models.Entities;

using UseCases.Contracts;

namespace Infrastructure
{
    internal class SiteRepository : RepositoryBase<Site>, ISiteRepository
    {
        public SiteRepository(DirectoryDbContext dbContext) : base(dbContext) { }
    }
}
