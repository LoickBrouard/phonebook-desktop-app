﻿
using models.Entities;

using UseCases;

namespace Infrastructure
{
    internal class EmployeeRepository : RepositoryBase<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(DirectoryDbContext dbContext) : base(dbContext) { }    
    }
}
