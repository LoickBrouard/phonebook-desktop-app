﻿using Microsoft.EntityFrameworkCore;

using UseCases;
using UseCases.Contracts;

namespace Infrastructure
{
    internal class UnitOfWork : IUnitOfWork
    {
        private readonly DirectoryDbContext _dbContext;
        private IEmployeeRepository? _employeeRepository;
        private ISiteRepository? _siteRepository;
        private IDepartmentRepository? _departmentRepository;

        public UnitOfWork(DirectoryDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEmployeeRepository EmployeeRepository
        {
            get
            {
                _employeeRepository ??= new EmployeeRepository(_dbContext);
                return _employeeRepository;
            }
            set
            {
                _employeeRepository = value;
            }
        }

        public IDepartmentRepository DepartmentRepository
        {
            get
            {
                _departmentRepository ??= new DepartmentRepository(_dbContext);
                return _departmentRepository;
            }
            set
            {
                _departmentRepository = value;
            }
        }

        public ISiteRepository SiteRepository
        {
            get
            {
                _siteRepository ??= new SiteRepository(_dbContext);
                return _siteRepository;
            }
            set
            {
                _siteRepository = value;
            }
        }

        public void EnsureCreated()
        {
            _dbContext.Database.EnsureCreated();
        }

        public void EnsureDeleted()
        {
            _dbContext.Database.EnsureDeleted();
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }
    }
}
