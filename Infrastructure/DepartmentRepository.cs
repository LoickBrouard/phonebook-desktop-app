﻿
using models.Entities;

using UseCases.Contracts;

namespace Infrastructure
{
    internal class DepartmentRepository : RepositoryBase<Department>, IDepartmentRepository
    {
        public DepartmentRepository(DirectoryDbContext dbContext) : base(dbContext) { }
    }
}
