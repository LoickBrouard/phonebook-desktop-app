﻿
using Microsoft.Extensions.DependencyInjection;

using UseCases;
using UseCases.Contracts;

namespace Infrastructure
{
    public static class InfrastructureServiceProvider
    {
        public static IServiceCollection AddInfrastructureServices(this IServiceCollection services)
        {
            return services
                .AddScoped<UserService>()
                .AddScoped<EmployeesService>()
                .AddScoped<DepartmentService>()
                .AddScoped<SiteService>()
                .AddScoped<DemoService>();
        }

        public static IServiceCollection AddEntityServices(this IServiceCollection services)
        {
            return services
                .AddScoped<DirectoryDbContext>()
                .AddScoped<IUnitOfWork, UnitOfWork>()
                .AddScoped<ISiteRepository, SiteRepository>()
                .AddScoped<IDepartmentRepository, DepartmentRepository>()
                .AddScoped<IEmployeeRepository, EmployeeRepository>();
        }
    }
}
