﻿using System.Linq.Expressions;

using Microsoft.EntityFrameworkCore;

using UseCases;

namespace Infrastructure
{
    internal abstract class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    {
        private readonly DirectoryDbContext _dbContext;

        public RepositoryBase(DirectoryDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async virtual Task<TEntity> CreateAsync(TEntity entity)
        {
            await _dbContext.Set<TEntity>().AddAsync(entity);
            _dbContext.SaveChanges();
            _dbContext.ChangeTracker.Clear();
            return entity;
        }

        public virtual void Delete(TEntity entity)
        {
            _dbContext.Set<TEntity>().Remove(entity);
            _dbContext.SaveChanges();
            _dbContext.ChangeTracker.Clear();
        }

        public virtual Task<List<TEntity>> GetAllAsync()
        {
            return _dbContext.Set<TEntity>().AsNoTracking().ToListAsync();
            
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return _dbContext.Set<TEntity>().AsNoTracking().AsEnumerable();
        }

        public virtual IEnumerable<TEntity> GetWithConditions(Expression<Func<TEntity, bool>> expression)
        {
            return _dbContext.Set<TEntity>().Where(expression).AsNoTracking().AsEnumerable();
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        public virtual TEntity Update(TEntity entity)
        {
            _dbContext.Set<TEntity>().Update(entity);
            _dbContext.SaveChanges();
            _dbContext.ChangeTracker.Clear();
            return entity;
        }
    }
}
