﻿
using Microsoft.EntityFrameworkCore;

using models.Entities;

namespace Infrastructure
{
    internal class DirectoryDbContext : DbContext
    {
        public DbSet<Employee>? Employees { get; set; }
        public DbSet<Department>? Department { get; set; }
        public DbSet<Site>? Sites { get; set; }

        public DirectoryDbContext() {}
        public DirectoryDbContext(DbContextOptions options) : base(options){}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite(@"Data Source=c:\Temp\DirectoryDb.db;");
            }
            base.OnConfiguring(optionsBuilder);
        }

        public class DirectoryContextFactory : IDbContextFactory<DirectoryDbContext>
        {
            public DirectoryDbContext CreateDbContext()
            {
                DbContextOptionsBuilder optionsBuilder = new DbContextOptionsBuilder<DirectoryDbContext>();
                optionsBuilder.UseSqlite(@"Data Source=c:\Temp\DirectoryDb.db;");
                
                return new DirectoryDbContext(optionsBuilder.Options);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            // Declaring primary keys
            modelBuilder.Entity<Employee>().HasKey(employee => employee.Id);
            modelBuilder.Entity<Department>().HasKey(department => department.Id);
            modelBuilder.Entity<Site>().HasKey(site => site.Id);


            // Constraints on Employee table
            modelBuilder.Entity<Employee>().Property(e => e.Name).IsRequired();
            modelBuilder.Entity<Employee>().Property(e => e.FamilyName).IsRequired();
            modelBuilder.Entity<Employee>().Property(e => e.Phone).IsRequired();


            // Constraints on Department table
            modelBuilder.Entity<Department>().Property(d => d.Type).IsRequired();
            
            // Constraints on Site table
            modelBuilder.Entity<Site>().Property(s => s.Name).IsRequired();
            modelBuilder.Entity<Site>().Property(s => s.Address).IsRequired();
            modelBuilder.Entity<Site>().Property(s => s.ZipCode).IsRequired();
            modelBuilder.Entity<Site>().Property(s => s.City).IsRequired();



            // Relation Many-to-One Department-Employee with foreignKey
            modelBuilder.Entity<Department>()
                .HasMany<Employee>()
                .WithOne()
                .OnDelete(DeleteBehavior.NoAction)
                .HasForeignKey(employee => employee.IdDepartment);

            // Relation Many-to-One Site-Department with foreignKey
            modelBuilder.Entity<Site>()
                .HasMany<Department>()
                .WithOne()
                .OnDelete(DeleteBehavior.NoAction)
                .HasForeignKey(department => department.IdSite);

        }
    }
}
