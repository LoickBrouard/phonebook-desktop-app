﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DirectoryUI.ViewModels;
using DirectoryUI.ViewModels.Objects;

using models.Entities;

using UseCases;

namespace DirectoryUI.Stores
{
    internal class EmployeeStore : ViewModelBase
    {
        private readonly EmployeesService _employeesService;

        private List<Employee> employees;
        public List<Employee> Employees
        {
            get
            {
                return employees;
            }
            private set
            {
                employees = value.ToList();
                OnPropertyChanged(nameof(Employees));
            }
        }

        public EmployeeStore(EmployeesService employeesService)
        {
            Employees = new List<Employee>();
            _employeesService = employeesService;
            _intializeLazy = new Lazy<Task>(Initialize);
        }

        #region Employee Storage handling

        private Lazy<Task> _intializeLazy;

        /// <summary>
        /// Called on the first time to get data from database
        /// </summary>
        /// <returns></returns>
        private async Task Initialize()
        {
            employees = await _employeesService.GetAllAsync();
            employees = employees.OrderBy(e => e.IdDepartment).ThenBy(e => e.FamilyName).ThenBy(e => e.Name).ToList();
        }

        /// <summary>
        /// The first time this function is called, the information will come from the database
        /// Then, the information will come from the memory cache
        /// </summary>
        /// <returns></returns>
        public async Task Load()
        {
            await _intializeLazy.Value;
        }

        /// <summary>
        /// Force the cache to be reseted with database values
        /// </summary>
        /// <returns></returns>
        public async Task ReLoad()
        {
            _intializeLazy = new Lazy<Task>(Initialize);
            await _intializeLazy.Value;
            OnPropertyChanged(nameof(Employees));
        }

        #endregion

        #region Selection of item in ListView

        private Employee employeeSelected;
        public EmployeeViewModel GetSelected() => (employeeSelected is null) ? null : new EmployeeViewModel(employeeSelected);

        public void AddToSelection(EmployeeViewModel employee)
        {
            if (employee != null)
                employeeSelected = employee.ConvertToEmployee();
            else
                employeeSelected = null;
        }

        #endregion

        #region Add Employee

        public event Action<Employee> EmployeeCreated;
        public async Task Add(Employee employee)
        {
            await _employeesService.AddAsync(employee);
            OnEmployeeCreated(employee);
        }
        private void OnEmployeeCreated(Employee employee)
        {
            employees.Add(employee);
            EmployeeCreated?.Invoke(employee);
        }

        #endregion

        #region Update Employee

        public event Action<Employee> EmployeeUpdated;
        public void Update(Employee employee)
        {
            if (employeeSelected == null)
            { return; }
            _employeesService.Update(employee);
            OnEmployeeUpdated(employee);
        }
        private void OnEmployeeUpdated(Employee employee)
        {
            employees[employees.FindIndex(e => e.Id == employee.Id)] = employee;
            EmployeeUpdated?.Invoke(employee);
        }

        #endregion

        #region Delete Employee

        public event Action<Employee> EmployeeDeleted;
        public void Delete()
        {
            if (employeeSelected == null)
            { return; }
            _employeesService.Delete(employeeSelected);
            OnEmployeeDeleted(employeeSelected);
        }
        private void OnEmployeeDeleted(Employee employee)
        {
            employees.RemoveAt(employees.FindIndex(e => e.Id == employee.Id));
            EmployeeDeleted?.Invoke(employee);
        }

        #endregion

        public string IsValid(Employee employee)
        {
            if (employee.IdDepartment.Equals(Guid.Empty))
                return "You need to choose a department";
            if (string.IsNullOrWhiteSpace(employee.Name))
                return "The Name is missing";
            if (string.IsNullOrWhiteSpace(employee.FamilyName))
                return "The Family Name is missing";
            if (string.IsNullOrWhiteSpace(employee.Phone))
                return "The Phone number is missing";

            return null;
        }

        public override void Dispose()
        {
            employeeSelected = null;
            base.Dispose();
        }
    }
}
