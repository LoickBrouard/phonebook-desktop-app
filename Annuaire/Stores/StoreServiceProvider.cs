﻿using Microsoft.Extensions.DependencyInjection;

namespace DirectoryUI.Stores
{
    internal static class ServiceProvider
    {
        public static IServiceCollection AddStoreServices(this IServiceCollection services)
        {
            return services
                .AddSingleton<EmployeeStore>()
                .AddSingleton<DepartmentStore>()
                .AddSingleton<SiteStore>()
                .AddSingleton<NavigationStore>()
                .AddSingleton<AccountStore>();
        }
    }
}
