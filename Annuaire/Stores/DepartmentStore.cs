﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

using DirectoryUI.ViewModels;
using DirectoryUI.ViewModels.Objects;

using models.Entities;

using UseCases;

namespace DirectoryUI.Stores
{
    internal class DepartmentStore : ViewModelBase
    {
        private readonly DepartmentService _departmentService;

        private List<Department> departments;
        public IEnumerable<Department> Departments
        {
            get
            {
                return departments;
            }
            private set
            {
                departments = value.ToList();
                OnPropertyChanged(nameof(Department));
            }
        }

        public DepartmentStore(DepartmentService departmentService)
        {
            Departments = new List<Department>();
            _departmentService = departmentService;
            _intializeLazy = new Lazy<Task>(Initialize);
        }

        public DepartmentViewModel GetDepartmentFromId(Guid guid) => new DepartmentViewModel(_departmentService.GetDepartmentFromId(guid));

        #region Department Storage handling

        private Lazy<Task> _intializeLazy;

        /// <summary>
        /// Called on the first time to get data from database
        /// </summary>
        /// <returns></returns>
        private async Task Initialize()
        {
            departments = await _departmentService.GetDepartmentsAsync();
        }

        /// <summary>
        /// The first time this function is called, the information will come from the database
        /// Then, the information will come from the memory cache
        /// </summary>
        /// <returns></returns>
        public async Task Load() => await _intializeLazy.Value;

        /// <summary>
        /// Force the cache to be reseted with database values
        /// </summary>
        /// <returns></returns>
        public async Task ReLoad()
        {
            _intializeLazy = new Lazy<Task>(Initialize);
            await _intializeLazy.Value;
            OnPropertyChanged(nameof(Departments));
        }

        #endregion

        #region Selection of item in listView

        private Department departmentSelected;

        public DepartmentViewModel GetSelected() => (departmentSelected is null) ? null : new DepartmentViewModel(departmentSelected);
        public void AddToSelection(DepartmentViewModel department)
        {
            if (department != null)
                departmentSelected = department.ConvertToDepartment();
            else
                departmentSelected = null;
        }

        #endregion

        #region Add Deppartment

        public event Action<Department> DepartmentCreated;
        private void OnDepartmentCreated(Department department)
        {
            departments.Add(department);
            DepartmentCreated?.Invoke(department);
        }
        public async Task Add(Department department)
        {
            await _departmentService.AddAsync(department);
            OnDepartmentCreated(department);
        }

        #endregion

        #region Update Department

        public event Action<Department> DepartmentUpdated;
        public void Update(Department department)
        {
            if (departmentSelected == null)
            { return; }
            _departmentService.Update(department);
            OnDepartmentUpdated(department);
        }
        private void OnDepartmentUpdated(Department department)
        {
            departments[departments.FindIndex(e => e.Id == department.Id)] = department;
            DepartmentUpdated?.Invoke(department);
        }

        #endregion

        #region Delete Department

        public event Action<Department> DepartmentDeleted;
        public void Delete()
        {
            if (departmentSelected == null)
            { return; }
            if (IsEmployeeAttached())
            {
                MessageBox.Show("You cannot delete a department full of employee");
                return;
            }

            _departmentService.Delete(departmentSelected);
            OnDepartmentDeleted(departmentSelected);

        }
        private void OnDepartmentDeleted(Department department)
        {
            departments.RemoveAt(departments.FindIndex(d => d.Id == department.Id));
            DepartmentDeleted?.Invoke(department);
        }

        #endregion

        /// <summary>
        /// Return false if there is employee attached to the department
        /// </summary>
        /// <returns></returns>
        public bool IsEmployeeAttached() => _departmentService.IsContainingEmployee(departmentSelected.Id);

        /// Return an error string if any
        /// </summary>
        /// <returns></returns>
        public string IsValid(Department department)
        {
            if (department.IdSite.Equals(Guid.Empty))
                return "You need to choose a site";
            if (string.IsNullOrWhiteSpace(department.Type))
                return "The type must be full filled";
            if (_departmentService.GetDepartmentFromName(department.Name) != null)
                return "This derpartment name already exists";

            return null;
        }


        /// Return an error string if any
        /// </summary>
        /// <returns></returns>
        public string IsValidForUpdate(Department department)
        {
            if (department.IdSite.Equals(Guid.Empty))
                return "You need to choose a site";
            if (string.IsNullOrWhiteSpace(department.Type))
                return "The type must be full filled";

            return null;
        }

        public override void Dispose()
        {
            departmentSelected = null;
            base.Dispose();
        }
    }
}
