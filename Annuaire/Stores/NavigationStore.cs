﻿using System;

using DirectoryUI.ViewModels;

namespace DirectoryUI.Stores
{
    public class NavigationStore
    {
        private ViewModelBase _currentViewModel;

        public ViewModelBase CurrentViewModel
        {
            get => _currentViewModel;
            set
            {
                _currentViewModel?.Dispose();
                _currentViewModel = value;
                OnCurrentViewModelChanged();
            }
        }

        public event Action CurrentViewModelChanged;

        private void OnCurrentViewModelChanged()
        {
            CurrentViewModelChanged?.Invoke();
        }

        #region Modal

        public bool ModalIsOpen => CurrentModalViewModel != null;

        private ViewModelBase _currentModalViewModel;
        public ViewModelBase CurrentModalViewModel
        {
            get => _currentModalViewModel;
            set
            {
                _currentModalViewModel = value;
                OnCurrentModalViewModelChanged();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public event Action CurrentModalViewModelChanged;
        private void OnCurrentModalViewModelChanged()
        {
            CurrentModalViewModelChanged?.Invoke();
        }

        public void Close()
        {
            CurrentModalViewModel = null;
            OnCurrentModalViewModelChanged();
        }

        #endregion
    }
}
