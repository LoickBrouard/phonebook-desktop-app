﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

using DirectoryUI.ViewModels;
using DirectoryUI.ViewModels.Objects;

using models.Entities;

using UseCases;

namespace DirectoryUI.Stores
{
    internal class SiteStore : ViewModelBase
    {
        private readonly SiteService _siteService;

        private List<Site> sites;
        public IEnumerable<Site> Sites
        {
            get
            {
                return sites;
            }
            private set
            {
                sites = value.ToList();
                OnPropertyChanged(nameof(Sites));
            }
        }


        public SiteStore(SiteService siteService)
        {
            Sites = new List<Site>();
            _siteService = siteService;
            _intializeLazy = new Lazy<Task>(Initialize);
        }

        public SiteViewModel GetSiteFromId(Guid guid) => new(_siteService.GetSiteFromId(guid));


        #region Site Storage handling

        private Lazy<Task> _intializeLazy;

        /// <summary>
        /// Called on the first time to get data from database
        /// </summary>
        /// <returns></returns>
        private async Task Initialize()
        {
            Sites = await _siteService.GetSitesAsync();
        }

        /// <summary>
        /// The first time this function is called, the information will come from the database
        /// Then, the information will come from the memory cache
        /// </summary>
        /// <returns></returns>
        public async Task Load()
        {
            await _intializeLazy.Value;
        }

        /// <summary>
        /// Force the cache to be reseted with database values
        /// </summary>
        /// <returns></returns>
        public async Task ReLoad()
        {
            _intializeLazy = new Lazy<Task>(Initialize);
            await _intializeLazy.Value;
            OnPropertyChanged(nameof(Sites));
        }

        #endregion

        #region Selection of item in listView

        private Site siteSelected;

        public SiteViewModel GetSelected() => (siteSelected is null) ? null : new SiteViewModel(siteSelected);

        public void AddToSelection(SiteViewModel site)
        {
            if (site != null)
                siteSelected = site.ConvertToSite();
            else
                siteSelected = null;
        }

        #endregion

        #region Add Site

        public event Action<Site> SiteCreated;
        public async Task Add(Site site)
        {
            await _siteService.AddAsync(site);
            OnSiteCreated(site);
        }
        private void OnSiteCreated(Site site)
        {
            sites.Add(site);
            SiteCreated?.Invoke(site);
        }

        #endregion

        #region Update Site

        public event Action<Site> SiteUpdated;
        public void Update(Site site)
        {
            if (siteSelected == null)
            { return; }
            _siteService.Update(site);
            OnSiteUpdated(site);
        }
        private void OnSiteUpdated(Site site)
        {
            sites[sites.FindIndex(e => e.Id == site.Id)] = site;
            SiteUpdated?.Invoke(site);
        }

        #endregion

        #region Delete Site

        public event Action<Site> SiteDeleted;
        public void Delete()
        {

            if (siteSelected == null)
            { return; }

            if (IsDepartmentAttached())
            {
                MessageBox.Show("You cannot delete a site with department attached");
                return;
            }

            _siteService.Delete(siteSelected);
            OnSiteDeleted(siteSelected);

        }
        private void OnSiteDeleted(Site site)
        {
            sites.RemoveAt(sites.FindIndex(s => s.Id == site.Id));
            SiteDeleted?.Invoke(site);
        }

        #endregion

        /// <summary>
        /// Return false if there is employee attached to the department
        /// </summary>
        /// <returns></returns>
        public bool IsDepartmentAttached() => _siteService.IsContainingDepartment(siteSelected.Id);

        /// Return an error string if any
        /// </summary>
        /// <returns></returns>
        public string IsValid(Site site)
        {
            if (string.IsNullOrWhiteSpace(site.Name))
                return "The Name is missing";
            if (string.IsNullOrWhiteSpace(site.Address))
                return "The Address is missing";
            if (string.IsNullOrWhiteSpace(site.ZipCode))
                return "The Zip Code is missing";
            if (string.IsNullOrWhiteSpace(site.City))
                return "The City is missing";
            if (_siteService.GetSiteFromName(site.Name) != null)
                return "This site name already exists";

            return null;
        }

        /// Return an error string if any
        /// </summary>
        /// <returns></returns>
        public string IsValidForUpdate(Site site)
        {
            if (string.IsNullOrWhiteSpace(site.Name))
                return "The Name is missing";
            if (string.IsNullOrWhiteSpace(site.Address))
                return "The Address is missing";
            if (string.IsNullOrWhiteSpace(site.ZipCode))
                return "The Zip Code is missing";
            if (string.IsNullOrWhiteSpace(site.City))
                return "The City is missing";

            return null;
        }

        public override void Dispose()
        {
            siteSelected = null;
        }

    }
}
