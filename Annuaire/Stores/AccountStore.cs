﻿using System;

using DirectoryUI.ViewModels.Objects;

using UseCases;

namespace DirectoryUI.Stores
{
    internal class AccountStore
    {
        private readonly UserService _userService;
        private UserViewModel _currentUser;
        public UserViewModel CurrentUser
        {
            get => _currentUser;
            set
            {
                _currentUser = value;
                CurrentUserChanged?.Invoke();
            }
        }
        public bool IsLoggedIn => CurrentUser != null;

        public event Action CurrentUserChanged;

        public AccountStore(UserService accountService)
        {
            _userService = accountService;
        }

        public bool Login(UserViewModel user)
        {
            UserViewModel userLoggedIn = UserViewModel.convertFromEntity(_userService.Connection(user.convertToEntity()));
            return (userLoggedIn != null);
        }

        public void Logout()
        {
            CurrentUser = null;
        }
    }
}
