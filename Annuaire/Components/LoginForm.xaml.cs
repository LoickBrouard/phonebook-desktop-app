﻿using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;

using DirectoryUI.ViewModels;

namespace DirectoryUI.Components
{
    /// <summary>
    /// Interaction logic for LoginForm.xaml
    /// </summary>
    public partial class LoginForm : UserControl
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (DataContext != null)
            { 
                ((LoginFormViewModel)DataContext).Password = ((PasswordBox)sender).Password;
            }
        }


        private void PasswordBox_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (DataContext != null)
            {
                ((PasswordBox)sender).Password = ((LoginFormViewModel)DataContext).Password;
            }
        }
    }
}
