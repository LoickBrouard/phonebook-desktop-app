﻿using System.Windows;
using System.Windows.Controls;

namespace DirectoryUI.Components
{
    /// <summary>
    /// Interaction logic for Layout.xaml
    /// </summary>
    public partial class Layout : UserControl
    {
        public Layout()
        {
            InitializeComponent();
        }
    }
}
