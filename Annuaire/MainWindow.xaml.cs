﻿using System.Windows;
using System.Windows.Input;

using DirectoryUI.Commands;

namespace DirectoryUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
    }
}
