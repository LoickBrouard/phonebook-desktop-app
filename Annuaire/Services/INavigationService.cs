﻿namespace DirectoryUI.Services
{
    /// <summary>
    /// The implementation of INavigation service allow to change the current view Model in the MainViewModel class
    /// </summary>
    public interface INavigationService
    {
        /// <summary>
        /// To change the current view Model in the MainViewModel class
        /// </summary>
        public void Navigate();
    }
}