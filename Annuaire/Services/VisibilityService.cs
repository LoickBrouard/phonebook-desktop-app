﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace DirectoryUI.Services
{
    /// <summary>
    /// Service overrinding the standard ConvertToBooleanConverter tool.
    /// It allows here to add a reverse parameter to switch beetwen visibility on the IsLoggedIn property
    /// Exemple : 
    ///     IsLoggedIn + Converter --> Visible
    ///     !IsLoggedIn + Converter --> Collapse
    ///     
    ///     IsLoggedIn + Converter "reverse" --> Collapse
    ///     !IsLoggedIn + Converter "reverse" --> Visible
    /// </summary>
    internal class VisibilityService : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool bValue = false;

            // To check if the property bound to the converter is a boolean
            if (value is bool boolean)
                bValue = boolean;

            // To prevent from nullable exception
            else if (value is bool?)
                bValue = (bool?)value ?? false;

            // To check if there is a ConverterParameter string
            if(parameter is string)
                if ("reverse" == parameter.ToString())
                    bValue = !bValue;

            // Return the visibility bound to the boolean
            return (bValue) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Visibility)
                return (Visibility)value == Visibility.Visible;

            else
                return false;

        }
    }
}
