﻿
using System;

using DirectoryUI.Stores;
using DirectoryUI.ViewModels;

namespace DirectoryUI.Services
{
    /// <summary>
    /// Specific implementation of INavigationService targeting the CurrentModalViewModel property instead of CurrentViewModel
    /// </summary>
    /// <typeparam name="TViewModel"></typeparam>
    internal class ModalNavigationService<TViewModel> : INavigationService where TViewModel : ViewModelBase
    {
        private readonly NavigationStore _navigationStore;
        private readonly Func<TViewModel> _createViewModel;

        public ModalNavigationService(NavigationStore navigationStore, Func<TViewModel> createViewModel)
        {
            _navigationStore = navigationStore;
            _createViewModel = createViewModel;
        }

        public void Navigate()
        {
            _navigationStore.CurrentModalViewModel = _createViewModel();
        }
    }
}
