﻿using DirectoryUI.Stores;

namespace DirectoryUI.Services
{
    internal class CloseModalNavigationService : INavigationService
    {
        private readonly NavigationStore _navigationStore;

        public CloseModalNavigationService(NavigationStore navigationStore)
        {
            _navigationStore = navigationStore;
        }

        public void Navigate()
        {
            _navigationStore.Close();
        }
    }
}
