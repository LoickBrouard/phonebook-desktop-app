﻿using System;

using DirectoryUI.Stores;
using DirectoryUI.ViewModels;

namespace DirectoryUI.Services
{
    /// <summary>
    /// Generic implementation of INavigationService, allowing to strongly typed the viewModel to the NavigationService
    /// In the NavigationServiceProvider, it allows to load Navigation to different ViewModel from the same interface/class
    /// </summary>
    /// <typeparam name="TViewModel"></typeparam>
    internal class NavigationService<TViewModel> : INavigationService where TViewModel : ViewModelBase
    {
        private readonly NavigationStore _navigationStore;
        private readonly Func<TViewModel> _createViewModel;
        private readonly Func<NavigationBarViewModel> _createNavigationBarViewModel;

        public NavigationService(NavigationStore navigationStore, Func<TViewModel> createViewModel, Func<NavigationBarViewModel> createNavigationBarViewModel)
        {
            _navigationStore = navigationStore;
            _createViewModel = createViewModel;
            _createNavigationBarViewModel = createNavigationBarViewModel;
        }

        public void Navigate()
        {
            _navigationStore.CurrentViewModel = new LayoutViewModel(
                _createViewModel(),
                _createNavigationBarViewModel());
        }
    }
}
