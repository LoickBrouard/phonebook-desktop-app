﻿using System;

using DirectoryUI.Stores;
using DirectoryUI.ViewModels;
using DirectoryUI.ViewModels.DepartmentViewModels;
using DirectoryUI.ViewModels.EmployeeViewModels;
using DirectoryUI.ViewModels.SiteViewModels;

using Microsoft.Extensions.DependencyInjection;

using UseCases;

namespace DirectoryUI.Services
{
    internal static  class NavigationServiceProvider
    {
        public static IServiceCollection AddNavigationServices(this IServiceCollection services)
        {
            return services
                .AddSingleton<INavigationService>(s => CreateHomeNavigationService(s))
                .AddTransient<NavigationBarViewModel>(CreateNavigationBarViewModel)
                .AddTransient<CloseModalNavigationService>();
        }

        public static  NavigationBarViewModel CreateNavigationBarViewModel(IServiceProvider serviceProvider)
        {
            return new NavigationBarViewModel(
                CreateHomeNavigationService(serviceProvider),
                CreateEmployeesListNavigationService(serviceProvider),
                CreateDepartmentListNavigationService(serviceProvider),
                CreateSitesListNavigationService(serviceProvider),
                serviceProvider.GetRequiredService<AccountStore>(),
                serviceProvider.GetRequiredService<EmployeeStore>(),
                serviceProvider.GetRequiredService<DepartmentStore>(),
                serviceProvider.GetRequiredService<SiteStore>(),
                serviceProvider.GetRequiredService<DemoService>());
        }

        public static INavigationService CreateHomeNavigationService(IServiceProvider serviceProvider)
        {
            return new NavigationService<HomeViewModel>(
                serviceProvider.GetRequiredService<NavigationStore>(),
                () => serviceProvider.GetRequiredService<HomeViewModel>(),
                () => serviceProvider.GetRequiredService<NavigationBarViewModel>());
        }

        public static INavigationService CreateEmployeesListNavigationService(IServiceProvider serviceProvider)
        {
            return new NavigationService<EmployeeListViewModel>(
                serviceProvider.GetRequiredService<NavigationStore>(),
                () => serviceProvider.GetRequiredService<EmployeeListViewModel>(),
                () => serviceProvider.GetRequiredService<NavigationBarViewModel>());
        }

        public static INavigationService CreateDepartmentListNavigationService(IServiceProvider serviceProvider)
        {
            return new NavigationService<DepartmentListViewModel>(
                serviceProvider.GetRequiredService<NavigationStore>(),
                () => serviceProvider.GetRequiredService<DepartmentListViewModel>(),
                () => serviceProvider.GetRequiredService<NavigationBarViewModel>());
        }

        public static INavigationService CreateSitesListNavigationService(IServiceProvider serviceProvider)
        {
            return new NavigationService<SiteListViewModel>(
                serviceProvider.GetRequiredService<NavigationStore>(),
                () => serviceProvider.GetRequiredService<SiteListViewModel>(),
                () => serviceProvider.GetRequiredService<NavigationBarViewModel>());
        }
    }
}
