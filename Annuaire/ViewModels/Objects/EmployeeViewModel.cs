﻿using System;

using models.Entities;

namespace DirectoryUI.ViewModels.Objects
{
    public class EmployeeViewModel : ViewModelBase
    {
        private readonly Employee _employee;

        public string Name
        {
            get
            {
                return _employee.Name;
            }
            set
            {
                _employee.Name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        public DateTime CreatedDate
        {
            get
            {
                return _employee.CreatedDate;
            }
        }

        public string FamilyName
        {
            get
            {
                return _employee.FamilyName;
            }
            set
            {
                _employee.FamilyName = value;
                OnPropertyChanged(nameof(FamilyName));
            }
        }

        public string Phone
        {
            get
            {
                return _employee.Phone;
            }
            set
            {
                _employee.Phone = value;
                OnPropertyChanged(nameof(Phone));
            }
        }

        public string CellPhone
        {
            get
            {
                return _employee.CellPhone;
            }
            set
            {
                _employee.CellPhone = value;
                OnPropertyChanged(nameof(CellPhone));
            }
        }

        public string Email
        {
            get
            {
                return _employee.Email;
            }
            set
            {
                _employee.Email = value;
                OnPropertyChanged(nameof(Email));
            }
        }
        private DepartmentViewModel _department;
        public DepartmentViewModel Department
        {
            get
            {
                return _department;
            }
            set
            {
                _department = value;
                OnPropertyChanged(nameof(Department));
            }
        }

        private SiteViewModel _site;
        public SiteViewModel Site
        {
            get
            {
                return _site;
            }
            set
            {
                _site = value;
                OnPropertyChanged(nameof(Site));
            }
        }
        public Guid IdDepartment
        {
            get
            {
                return _employee.IdDepartment;
            }
            set
            {
                _employee.IdDepartment = value;
                OnPropertyChanged(nameof(IdDepartment));
            }
        }

        public EmployeeViewModel()
        {
            _employee = new Employee();
        }

        public EmployeeViewModel(Employee employee)
        {
            _employee = employee;
        }

        public Employee ConvertToEmployee()
        {
            return new Employee(_employee.Id, _employee.Name, _employee.FamilyName, _employee.Email, _employee.IdDepartment, _employee.Phone, _employee.CellPhone);
        }

    }
}
