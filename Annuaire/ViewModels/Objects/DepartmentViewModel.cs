﻿using System;

using models.Entities;

namespace DirectoryUI.ViewModels.Objects
{
    public class DepartmentViewModel : ViewModelBase
    {
        private readonly Department _department;

        public string Name { 
            get 
            {
                return _department.Name;
            }
            set 
            {
                _department.Name = value;
                OnPropertyChanged(nameof(Name));
            } 
        }

        public string Type
        {
            get
            {
                return _department.Type;
            }
            set
            {
                _department.Type = value;
                OnPropertyChanged(nameof(Type));
            }
        }
        private SiteViewModel _site;
        public SiteViewModel Site
        {
            get
            {
                return _site;
            }
            set
            {
                _site = value;
                OnPropertyChanged(nameof(Site));
            }
        }

        public Guid IdSite
        {
            get
            {
                return _department.IdSite;
            }
            set
            {
                _department.IdSite = value;
                OnPropertyChanged(nameof(IdSite));
            }
        }

        public DepartmentViewModel(Department department)
        {
            _department = department;
        }

        public DepartmentViewModel()
        {
            _department = new Department();
        }

        public Department ConvertToDepartment()
        {
            return new Department(_department.Id, _department.Name, _department.Type, _department.IdSite);
        }


    }
}
