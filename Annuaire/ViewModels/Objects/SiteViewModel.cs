﻿using models.Entities;

namespace DirectoryUI.ViewModels.Objects
{
    public class SiteViewModel : ViewModelBase
    {
        private readonly Site _site;

        public string Name { 
            get 
            {
                return _site.Name;
            }
            set 
            {
                _site.Name = value;
                OnPropertyChanged(nameof(Name));
            } 
        }

        public string Address
        {
            get
            {
                return _site.Address;
            }
            set
            {
                _site.Address = value;
                OnPropertyChanged(nameof(Address));
            }
        }
        public string ZipCode
        {
            get
            {
                return _site.ZipCode;
            }
            set
            {
                _site.ZipCode = value;
                OnPropertyChanged(nameof(ZipCode));
            }
        }
        public string City
        {
            get
            {
                return _site.City;
            }
            set
            {
                _site.City = value;
                OnPropertyChanged(nameof(City));
            }
        }

        public SiteViewModel()
        {
            _site = new Site();
        }

        public SiteViewModel(Site site)
        {
            _site = site;
        }

        public Site ConvertToSite()
        {
            return new Site(_site.Id, _site.Name, _site.Address, _site.ZipCode, _site.City);
        } 


    }
}
