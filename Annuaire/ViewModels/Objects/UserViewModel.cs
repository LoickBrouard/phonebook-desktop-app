﻿using System;

using models.Entities;

namespace DirectoryUI.ViewModels.Objects
{
    internal class UserViewModel: ViewModelBase
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public UserViewModel(string username, string password, bool authentication = false)
        {
            Username = username;
            Password = password;
        }
        
        public User convertToEntity()
        {
            return new User(Username, Password);
        }

        public static UserViewModel convertFromEntity(User user)
        {
            return new UserViewModel(user.Username, user.Password);
        }
    }
}
