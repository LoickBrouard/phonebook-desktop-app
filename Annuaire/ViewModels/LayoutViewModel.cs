﻿

namespace DirectoryUI.ViewModels
{
    /// <summary>
    /// Called by MainViewModel. The layout display the NavigationBar View along the Custom View
    /// </summary>
    internal class LayoutViewModel : ViewModelBase
    {
        public LayoutViewModel(ViewModelBase viewModelContained, NavigationBarViewModel navigationBarViewModel)
        {
            ViewModelContained = viewModelContained;
            NavigationBarViewModel = navigationBarViewModel;
        }

        public NavigationBarViewModel NavigationBarViewModel { get; }

        public ViewModelBase ViewModelContained { get; } = new HomeViewModel();

        /// <summary>
        /// Dispose the view releasing all classes kept alive by an event
        /// </summary>
        public override void Dispose()
        {
            NavigationBarViewModel.Dispose();
            ViewModelContained.Dispose();
            base.Dispose();
        }
    }
}
