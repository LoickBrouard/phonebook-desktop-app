﻿using System;
using System.Configuration;
using System.Windows;
using System.Windows.Input;

using DirectoryUI.Commands;
using DirectoryUI.Stores;
using DirectoryUI.ViewModels.Objects;

using Utilities;

namespace DirectoryUI.ViewModels
{
    internal class LoginFormViewModel : ViewModelBase
    {
        public override string Header => "Admin Login";

        private bool showPassword;
        public bool ShowPassword
        {
            get
            {
                return showPassword;
            }
            set
            {
                showPassword = value;
                OnPropertyChanged(nameof(ShowPassword));

            }
        }
        public ICommand ShowPasswordCommand { get; set; }
        public ICommand ConnectCommand { get; set; }

        private string _userName;
        public string Username
        {
            get => _userName;

            set
            {
                _userName = value;
                OnPropertyChanged(nameof(Username));
}
}

        private string _password;

        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                OnPropertyChanged(nameof(Password));
            }
        }

        private readonly AccountStore _accountStore;
        private readonly NavigationStore _navigationStore;
        public LoginFormViewModel(AccountStore accountStore, NavigationStore navigationStore)
        {
            Password = string.Empty;
            Username = string.Empty;
            ConnectCommand = new RelayCommand(() => login());
            ShowPasswordCommand = new RelayCommand(() => ShowPassword = !ShowPassword);
            _accountStore = accountStore;
            _navigationStore = navigationStore;
        }

        private void login()
        {
            // Get the references passwords
            string userReference = ConfigurationManager.AppSettings["AdminUser"].ToString();
            string passwordReference = ConfigurationManager.AppSettings["AdminPassWord"].ToString();

            if (!Username.VerifyHash(userReference))
            {
                MessageBox.Show("Username is not valid");
                return;
            }
            if (!Password.VerifyHash(passwordReference))
            {
                MessageBox.Show("Password is not valid");
                return;
            }
            UserViewModel userTried = new(Username, Password.ToString());

            Username = string.Empty;
            Password = string.Empty;
            _accountStore.CurrentUser = userTried;
            _navigationStore.Close();
        }
    }
}
