﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

using DirectoryUI.Commands;
using DirectoryUI.Services;
using DirectoryUI.Stores;
using DirectoryUI.ViewModels.Objects;

using models.Entities;

namespace DirectoryUI.ViewModels.DepartmentViewModels
{
    internal class UpdateDepartmentViewModel : ViewModelBase
    {
        public override string Header => "Update new Department";


        private readonly DepartmentStore _departmentStore;
        private readonly SiteStore _siteStore;
        private readonly INavigationService _closeNavigationService;

        private readonly DepartmentViewModel _department;

        private readonly List<Site> _sites;
        public List<Site> Sites => _sites;

        private string _departmentName;
        public string DepartmentName
        {
            get
            {
                _departmentName = $"{Sites.Find(s => s.Id == idSiteSelected)?.Name.Substring(0, 3)}_{_departmentType}";
                return _departmentName;
            }
            set
            {
                _departmentName = $"{Sites.Find(s => s.Id == idSiteSelected)?.Name.Substring(0, 3)}_{_departmentType}";
                OnPropertyChanged(nameof(DepartmentName));
            }
        }

        private string _departmentType;
        public string DepartmentType
        {
            get
            {
                return _departmentType;
            }
            set
            {
                _departmentType = value;
                OnPropertyChanged(nameof(DepartmentType));
                OnPropertyChanged(nameof(DepartmentName));
            }
        }

        private Guid idSiteSelected;
        public Guid IdSiteSelected
        {
            get
            {
                return idSiteSelected;
            }
            set
            {
                idSiteSelected = value;
                OnPropertyChanged(nameof(IdSiteSelected));
                OnPropertyChanged(nameof(DepartmentName));
            }
        }

        public ICommand SubmitCommand { get; }
        public ICommand CancelCommand { get; }

        public UpdateDepartmentViewModel(INavigationService closeNavigationService, DepartmentStore departmentStore, SiteStore siteStore)
        {

            _closeNavigationService = closeNavigationService;
            _departmentStore = departmentStore;
            _siteStore = siteStore;
            _sites = _siteStore.Sites.ToList();
            LoadUpdateViewModel();
            
            // If no entry was selected
            if (_departmentStore.GetSelected() == null)
            {
                _closeNavigationService.Navigate();
                return;
            }
            _department = new DepartmentViewModel(_departmentStore.GetSelected().ConvertToDepartment());


            IdSiteSelected = _department.IdSite;
            DepartmentType = _department.Type;
            DepartmentName = _department.Name;
            SubmitCommand = new RelayCommand(Update);
            CancelCommand = new NavigateCommand(closeNavigationService);
        }

        public async void LoadUpdateViewModel()
        {
            await _departmentStore.Load();
            await _siteStore.Load();
        }

        private void Update()
        {
            _department.Name = _departmentName;
            _department.Type = _departmentType;
            _department.IdSite = idSiteSelected;

            if (_departmentStore.IsEmployeeAttached())
            {
                MessageBox.Show("The Department must be empty to be renamed");
                return;
            }

            string messageErrorOnUpdate = _departmentStore.IsValidForUpdate(_department.ConvertToDepartment());
            if (!string.IsNullOrWhiteSpace(messageErrorOnUpdate))
            {
                MessageBox.Show(messageErrorOnUpdate);
                return;
            }

            _departmentStore.Update(_department.ConvertToDepartment());
            _closeNavigationService.Navigate();


        }
    }
}
