﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;

using DirectoryUI.Commands;
using DirectoryUI.Services;
using DirectoryUI.Stores;
using DirectoryUI.ViewModels.Objects;

using models.Entities;

namespace DirectoryUI.ViewModels.DepartmentViewModels
{
    internal class DepartmentListViewModel : ViewModelBase
    {
        public override string Header => "Departments";

        #region Admin

        public ICommand AddDepartmentNavigationCommand { get; set; }
        public ICommand DeleteDepartmentCommand { get; set; }
        public ICommand UpdateDepartmentNavigationCommand { get; set; }
        public bool IsLoggedIn => _accountStore.IsLoggedIn;

        private void OnCurrentUserChanged() => OnPropertyChanged(nameof(IsLoggedIn));

        private void OnDepartmentCreated(Department department)
        {
            _departments.Add(new DepartmentViewModel(department));
            LoadViewModel();
        }

        private void OnDepartmentUpdated(Department department)
        {
            _departments[_departments.IndexOf(SelectedDepartment)] = new DepartmentViewModel(department);
            LoadViewModel();
        }

        private void OnDepartmentDeleted(Department department) => _departments.Remove(SelectedDepartment);

        #endregion

        #region Stores

        private readonly DepartmentStore _departmentStore;
        private readonly SiteStore _siteStore;
        private readonly AccountStore _accountStore;

        #endregion

        #region Lists

        private DepartmentViewModel selectedDepartment;
        public DepartmentViewModel SelectedDepartment
        {
            get => selectedDepartment;
            set
            {
                if (IsLoggedIn)
                {
                    selectedDepartment = value;
                    OnPropertyChanged(nameof(SelectedDepartment));
                    _departmentStore.AddToSelection(selectedDepartment);
                }
            }
        }

        private readonly ObservableCollection<DepartmentViewModel> _departments;

        public IEnumerable<DepartmentViewModel> Departments => _departments; // Clone of the ObservableCollection for better encapsulation

        #endregion

        public DepartmentListViewModel(DepartmentStore departmentStore, AccountStore accountStore, INavigationService addDepartmentNavigationService, INavigationService updateDepartmentNavigationService, SiteStore siteStore)
        {
            _departments = new();
            _departmentStore = departmentStore;
            _siteStore = siteStore;
            _accountStore = accountStore;
            _departmentStore.AddToSelection(null);
            LoadViewModel();
            _departmentStore.DepartmentCreated += OnDepartmentCreated;
            _departmentStore.DepartmentUpdated += OnDepartmentUpdated;
            _departmentStore.DepartmentDeleted += OnDepartmentDeleted;
            _accountStore.CurrentUserChanged += OnCurrentUserChanged;

            AddDepartmentNavigationCommand = new NavigateCommand(addDepartmentNavigationService);
            UpdateDepartmentNavigationCommand = new NavigateCommand(updateDepartmentNavigationService);
            DeleteDepartmentCommand = new RelayCommand(() => _departmentStore.Delete());
        }

        /// <summary>
        /// Loading asynchronously stores and lists 
        /// </summary>
        public async void LoadViewModel()
        {
            await _departmentStore.Load();
            await _siteStore.Load();
            _departments.Clear();
            foreach (Department department in _departmentStore.Departments)
            {
                DepartmentViewModel departmentViewModel = new(department);
                departmentViewModel.Site = _siteStore.GetSiteFromId(departmentViewModel.IdSite);
                _departments.Add(departmentViewModel); 
            }
        }

        /// <summary>
        /// Dispose the view releasing all classes kept alive by an event
        /// </summary>
        public override void Dispose()
        {
            _departmentStore.DepartmentCreated -= OnDepartmentCreated;
            _departmentStore.DepartmentUpdated -= OnDepartmentUpdated;
            _departmentStore.DepartmentDeleted -= OnDepartmentDeleted;
            _accountStore.CurrentUserChanged -= OnCurrentUserChanged;
            base.Dispose();
        }
    }
}
