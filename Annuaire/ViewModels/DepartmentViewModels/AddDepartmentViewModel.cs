﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

using DirectoryUI.Commands;
using DirectoryUI.Services;
using DirectoryUI.Stores;
using DirectoryUI.ViewModels.Objects;

using models.Entities;

namespace DirectoryUI.ViewModels.DepartmentViewModels
{
    internal class AddDepartmentViewModel : ViewModelBase
    {
        public override string Header => "Add new Department";


        private readonly DepartmentStore _departmentStore;
        private readonly SiteStore _siteStore;
        private readonly INavigationService _closeNavigationService;

        public List<Site> Sites => _sites;
        private readonly List<Site> _sites;

        private string _departmentName;
        public string DepartmentName
        {
            get
            {
                _departmentName = $"{Sites.Find(s => s.Id == idSiteSelected)?.Name[..3]}_{_departmentType}";
                return _departmentName;
            }
            set
            {
                _departmentName = $"{Sites.Find(s => s.Id == idSiteSelected)?.Name[..3]}_{_departmentType}" ;
                OnPropertyChanged(nameof(DepartmentName));
            }
        }

        private string _departmentType;
        public string DepartmentType
        {
            get
            {
                return _departmentType;
            }
            set
            {
                _departmentType = value;
                OnPropertyChanged(nameof(DepartmentType));
                OnPropertyChanged(nameof(DepartmentName));
            }
        }

        private Guid idSiteSelected;
        public Guid IdSiteSelected { 
            get 
            { 
                return idSiteSelected; 
            } 
            set 
            { 
                idSiteSelected = value;
                OnPropertyChanged(nameof(IdSiteSelected)); 
                OnPropertyChanged(nameof(DepartmentName)); 
            } 
        }


        public DepartmentViewModel Department => _department;
        private readonly DepartmentViewModel _department = new();

        public ICommand SubmitCommand { get; }
        public ICommand CancelCommand { get; }

        public AddDepartmentViewModel(INavigationService closeNavigationService, DepartmentStore departmentStore, SiteStore siteStore)
        {

            _closeNavigationService = closeNavigationService;
            _departmentStore = departmentStore;
            _siteStore = siteStore;

            LoadAddViewModel();
            _sites = _siteStore.Sites.ToList();
            SubmitCommand = new AsyncRelayCommand(Add);
            CancelCommand = new NavigateCommand(closeNavigationService);
        }

        public async void LoadAddViewModel()
        {
            await _siteStore.Load();
        }

        private async Task Add()
        {
            Department departmentToAdd = new Department()
            { 
                Id = Guid.NewGuid(),
                Name = _departmentName,
                Type = _departmentType,
                IdSite = idSiteSelected
            };

            string message = _departmentStore.IsValid(departmentToAdd);
            if (string.IsNullOrWhiteSpace(message))
            {
                await _departmentStore.Add(departmentToAdd);
                _closeNavigationService.Navigate();
            }
            else
                MessageBox.Show(message);
        }
    }
}
