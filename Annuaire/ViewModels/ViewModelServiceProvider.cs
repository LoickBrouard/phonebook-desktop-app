﻿using System;

using DirectoryUI.Services;
using DirectoryUI.Stores;
using DirectoryUI.ViewModels.DepartmentViewModels;
using DirectoryUI.ViewModels.EmployeeViewModels;
using DirectoryUI.ViewModels.SiteViewModels;

using Microsoft.Extensions.DependencyInjection;

namespace DirectoryUI.ViewModels
{
    internal static class ViewModelServiceProvider
    {
        public static IServiceCollection AddViewModelServices(this IServiceCollection services)
        {
            return services
                .AddTransient<EmployeeListViewModel>(s => CreateEmployeeListViewModel(s))
                .AddTransient<DepartmentListViewModel>(s => CreateDepartmentListViewModel(s))
                .AddTransient<SiteListViewModel>(s => CreateSiteListViewModel(s))
                .AddTransient<HomeViewModel>()
                .AddTransient<LoginFormViewModel>()
                .AddTransient<AddEmployeeViewModel>(s => CreateAddEmployeeListViewModel(s))
                .AddTransient<UpdateEmployeeViewModel>(s => CreateUpdateEmployeeListViewModel(s))
                .AddTransient<AddDepartmentViewModel>(s => CreateAddDepartmentListViewModel(s))
                .AddTransient<UpdateDepartmentViewModel>(s => CreateUpdateDepartmentListViewModel(s))
                .AddTransient<AddSiteViewModel>(s => CreateAddSiteListViewModel(s))
                .AddTransient<UpdateSiteViewModel>(s => CreateUpdateSiteListViewModel(s))
                .AddSingleton<MainViewModel>()
                .AddSingleton(s => new MainWindow()
                {
                    DataContext = s.GetRequiredService<MainViewModel>()
                });
        }

        #region EmployeeViewModel collections

        private static EmployeeListViewModel CreateEmployeeListViewModel(IServiceProvider s)
        {
            return new EmployeeListViewModel(
                    s.GetRequiredService<EmployeeStore>(),
                    s.GetRequiredService<AccountStore>(),
                    new ModalNavigationService<AddEmployeeViewModel>(
                        s.GetRequiredService<NavigationStore>(),
                        () => s.GetRequiredService<AddEmployeeViewModel>()),
                     new ModalNavigationService<UpdateEmployeeViewModel>(
                        s.GetRequiredService<NavigationStore>(),
                        () => s.GetRequiredService<UpdateEmployeeViewModel>()),
                     s.GetRequiredService<DepartmentStore>(),
                     s.GetRequiredService<SiteStore>()
                    );
        }

        private static AddEmployeeViewModel CreateAddEmployeeListViewModel(IServiceProvider s)
        {
            return new AddEmployeeViewModel(
                    s.GetRequiredService<CloseModalNavigationService>(),
                    s.GetRequiredService<EmployeeStore>(),
                    s.GetRequiredService<DepartmentStore>()
                    );
        }

        private static UpdateEmployeeViewModel CreateUpdateEmployeeListViewModel(IServiceProvider s)
        {
            return new UpdateEmployeeViewModel(
                    s.GetRequiredService<CloseModalNavigationService>(),
                    s.GetRequiredService<EmployeeStore>(),
                    s.GetRequiredService<DepartmentStore>()
                    );
        }

        #endregion


        #region DepartmentViewModel collections

        private static DepartmentListViewModel CreateDepartmentListViewModel(IServiceProvider s)
        {
            return new DepartmentListViewModel(
                    s.GetRequiredService<DepartmentStore>(),
                    s.GetRequiredService<AccountStore>(),
                    new ModalNavigationService<AddDepartmentViewModel>(
                        s.GetRequiredService<NavigationStore>(),
                        () => s.GetRequiredService<AddDepartmentViewModel>()),
                     new ModalNavigationService<UpdateDepartmentViewModel>(
                        s.GetRequiredService<NavigationStore>(),
                        () => s.GetRequiredService<UpdateDepartmentViewModel>()),
                     s.GetService<SiteStore>()
                    );
        }

        private static AddDepartmentViewModel CreateAddDepartmentListViewModel(IServiceProvider s)
        {
            return new AddDepartmentViewModel(
                    s.GetRequiredService<CloseModalNavigationService>(),
                    s.GetRequiredService<DepartmentStore>(),
                    s.GetRequiredService<SiteStore>()
                    );
        }

        private static UpdateDepartmentViewModel CreateUpdateDepartmentListViewModel(IServiceProvider s)
        {
            return new UpdateDepartmentViewModel(
                    s.GetRequiredService<CloseModalNavigationService>(),
                    s.GetRequiredService<DepartmentStore>(),
                    s.GetRequiredService<SiteStore>()
                    );
        }

        #endregion


        #region SiteViewModel collections

        private static SiteListViewModel CreateSiteListViewModel(IServiceProvider s)
        {
            return new SiteListViewModel(
                    s.GetRequiredService<SiteStore>(),
                    s.GetRequiredService<AccountStore>(),
                    new ModalNavigationService<AddSiteViewModel>(
                        s.GetRequiredService<NavigationStore>(),
                        () => s.GetRequiredService<AddSiteViewModel>()),
                     new ModalNavigationService<UpdateSiteViewModel>(
                        s.GetRequiredService<NavigationStore>(),
                        () => s.GetRequiredService<UpdateSiteViewModel>())
                    );
        }

        private static AddSiteViewModel CreateAddSiteListViewModel(IServiceProvider s)
        {
            return new AddSiteViewModel(
                    s.GetRequiredService<CloseModalNavigationService>(),
                    s.GetRequiredService<SiteStore>()
                    );
        }

        private static UpdateSiteViewModel CreateUpdateSiteListViewModel(IServiceProvider s)
        {
            return new UpdateSiteViewModel(
                    s.GetRequiredService<CloseModalNavigationService>(),
                    s.GetRequiredService<SiteStore>());
        }

        #endregion
    }
}
