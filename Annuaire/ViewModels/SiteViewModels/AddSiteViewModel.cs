﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

using DirectoryUI.Commands;
using DirectoryUI.Services;
using DirectoryUI.Stores;

using DirectoryUI.ViewModels.Objects;

using models.Entities;

namespace DirectoryUI.ViewModels.SiteViewModels
{
    internal class AddSiteViewModel : ViewModelBase
    {
        public override string Header => "Add new Site";


        private readonly SiteStore _siteStore;
        private readonly INavigationService _closeNavigationService;

        public SiteViewModel Site => _site;
        private readonly SiteViewModel _site = new();

        public ICommand SubmitCommand { get; }
        public ICommand CancelCommand { get; }

        public AddSiteViewModel(INavigationService closeNavigationService, SiteStore siteStore)
        {

            _closeNavigationService = closeNavigationService;
            _siteStore = siteStore;

            SubmitCommand = new AsyncRelayCommand(Add);
            CancelCommand = new NavigateCommand(closeNavigationService);
        }

        private async Task Add()
        {
            Site siteToAdd = Site.ConvertToSite();
            string message = _siteStore.IsValid(siteToAdd);
            if (string.IsNullOrWhiteSpace(message))
            {
                await _siteStore.Add(siteToAdd);
                _closeNavigationService.Navigate();
            }
            else
                MessageBox.Show(message);

        }
    }
}
