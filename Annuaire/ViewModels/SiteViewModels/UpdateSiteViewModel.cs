﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

using DirectoryUI.Commands;
using DirectoryUI.Services;
using DirectoryUI.Stores;
using DirectoryUI.ViewModels.Objects;

using models.Entities;

namespace DirectoryUI.ViewModels.SiteViewModels
{
    internal class UpdateSiteViewModel : ViewModelBase
    {
        public override string Header => "Update new Site";


        private readonly SiteStore _siteStore;
        private readonly INavigationService _closeNavigationService;

        private readonly SiteViewModel _site;
        public SiteViewModel SiteUpdate => _site;


        public ICommand SubmitCommand { get; }
        public ICommand CancelCommand { get; }

        public UpdateSiteViewModel(INavigationService closeNavigationService, SiteStore siteStore)
        {

            _closeNavigationService = closeNavigationService;
            _siteStore = siteStore;
            

            LoadUpdateViewModel();
            // If no entry was selected
            if (_siteStore.GetSelected() == null)
            {
                _closeNavigationService.Navigate();
                return;
            }
            _site = new SiteViewModel(_siteStore.GetSelected().ConvertToSite());

            SubmitCommand = new RelayCommand(Update);
            CancelCommand = new NavigateCommand(closeNavigationService);
        }

        public async void LoadUpdateViewModel()
        {
            await _siteStore.Load();
        }

        private void Update()
        {
            Site siteToUpdate = SiteUpdate.ConvertToSite();

            if (_siteStore.IsDepartmentAttached())
            {
                MessageBox.Show("The site must be empty to be renamed");
                return;
            }

            string messageErrorOnUpdate = _siteStore.IsValidForUpdate(siteToUpdate);

            if (!string.IsNullOrWhiteSpace(messageErrorOnUpdate))
            {
                MessageBox.Show(messageErrorOnUpdate);
                return;
            }

            _siteStore.Update(SiteUpdate.ConvertToSite());
            _closeNavigationService.Navigate();

        }
    }
}
