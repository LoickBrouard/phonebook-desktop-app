﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;

using DirectoryUI.Commands;
using DirectoryUI.Services;
using DirectoryUI.Stores;
using DirectoryUI.ViewModels.Objects;

using models.Entities;

using UseCases;

namespace DirectoryUI.ViewModels.SiteViewModels
{
    internal class SiteListViewModel : ViewModelBase
    {

        public override string Header => "Sites";

        #region Admin

        public ICommand AddSiteNavigationCommand { get; set; }
        public ICommand DeleteSiteCommand { get; set; }
        public ICommand UpdateSiteNavigationCommand { get; set; }
        public bool IsLoggedIn => _accountStore.IsLoggedIn;

        private void OnCurrentUserChanged() => OnPropertyChanged(nameof(IsLoggedIn));

        private void OnSiteCreated(Site site)
        {
            _sites.Add(new SiteViewModel(site));
            LoadViewModel();
        }

        private void OnSiteUpdated(Site site)
        {
            _sites[_sites.IndexOf(SelectedSite)] = new SiteViewModel(site);
            LoadViewModel();
        }

        private void OnSiteDeleted(Site site)
        {
            _sites.Remove(SelectedSite);
        }

        #endregion

        private SiteViewModel selectedSite;
        public SiteViewModel SelectedSite
        {
            get => selectedSite;
            set
            {
                if (IsLoggedIn)
                {
                    selectedSite = value;
                    OnPropertyChanged(nameof(SelectedSite));
                    _siteStore.AddToSelection(selectedSite);
                }
            }
        }


        private ObservableCollection<SiteViewModel> _sites = new();
        public IEnumerable<SiteViewModel> Sites => _sites; // Clone of the ObservableCollection for better encapsulation

        private readonly SiteStore _siteStore;
        private readonly AccountStore _accountStore;


        public SiteListViewModel(SiteStore siteStore, AccountStore accountStore, INavigationService addSiteNavigationService, INavigationService updateSiteNavigationService)
        {
            _siteStore = siteStore;
            _accountStore = accountStore;
            _siteStore.AddToSelection(null);
            LoadViewModel();
            _siteStore.SiteCreated += OnSiteCreated;
            _siteStore.SiteUpdated += OnSiteUpdated;
            _siteStore.SiteDeleted += OnSiteDeleted;
            _accountStore.CurrentUserChanged += OnCurrentUserChanged;

            AddSiteNavigationCommand = new NavigateCommand(addSiteNavigationService);
            UpdateSiteNavigationCommand = new NavigateCommand(updateSiteNavigationService);
            DeleteSiteCommand = new RelayCommand(() => _siteStore.Delete());
        }


        public async void LoadViewModel()
        {
            await _siteStore.Load();
            _sites.Clear();
            foreach (Site site in _siteStore.Sites)
                _sites.Add(new SiteViewModel(site));
        }

        public override void Dispose()
        {
            _siteStore.SiteCreated -= OnSiteCreated;
            _siteStore.SiteUpdated -= OnSiteUpdated;
            _siteStore.SiteDeleted -= OnSiteDeleted;
            _accountStore.CurrentUserChanged -= OnCurrentUserChanged;
            base.Dispose();
        }
    }
}
