﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace DirectoryUI.ViewModels
{
    /// <summary>
    /// All viewModel inherit from this view. This allow to refactor the Header and the OnPropertyChanged method
    /// </summary>
    public abstract class ViewModelBase : INotifyPropertyChanged, IDisposable
    {
        public virtual string Header { get; } = "Phone Book";

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public virtual void Dispose() { }
    }
}
