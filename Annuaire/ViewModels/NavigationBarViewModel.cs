﻿using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Navigation;

using DirectoryUI.Commands;
using DirectoryUI.Services;
using DirectoryUI.Stores;

using UseCases;

namespace DirectoryUI.ViewModels
{
    internal class NavigationBarViewModel : ViewModelBase
    {
        #region Commands

        public ICommand NavigateHomeCommand { get; }
        public ICommand NavigateEmployeesListCommand { get; }
        public ICommand NavigateServicesListCommand { get; }
        public ICommand NavigateSitesListCommand { get; }
        public ICommand LoadDemoCommand { get; }
        public ICommand LogoutCommand { get; }

        private async void LoadDemo()
        {
            _demoService.LoadDemo();
            await _employeesStore.ReLoad();
            await _departmentStore.ReLoad();
            await _sitesSiteStore.ReLoad();

        }

        private void logout() => _accountStore.CurrentUser = null;

        #endregion

        #region Stores

        private readonly AccountStore _accountStore;
        private readonly EmployeeStore _employeesStore;
        private readonly DepartmentStore _departmentStore;
        private readonly SiteStore _sitesSiteStore;

        #endregion

        #region Services

        private readonly DemoService _demoService;

        #endregion

        public bool IsLoggedIn => _accountStore.IsLoggedIn;


        public NavigationBarViewModel(INavigationService homeNavigationService,
            INavigationService employeesListNavigationService,
            INavigationService servicesListNavigationService,
            INavigationService sitesNavigationService,
            AccountStore accountStore,
            EmployeeStore employeesStore,
            DepartmentStore departmentStore,
            SiteStore sitesSiteStore,
            DemoService demoService
            )
        {
            _demoService = demoService;
            _accountStore = accountStore;
            _employeesStore = employeesStore;
            _departmentStore = departmentStore;
            _sitesSiteStore = sitesSiteStore;
            _accountStore.CurrentUserChanged += OnCurrentUserChanged;

            NavigateHomeCommand = new NavigateCommand(homeNavigationService);
            NavigateEmployeesListCommand = new NavigateCommand(employeesListNavigationService);
            NavigateServicesListCommand = new NavigateCommand(servicesListNavigationService);
            NavigateSitesListCommand = new NavigateCommand(sitesNavigationService);
            LoadDemoCommand = new RelayCommand(() => LoadDemo());
            LogoutCommand = new RelayCommand(() => logout());
        }

        private void OnCurrentUserChanged() => OnPropertyChanged(nameof(IsLoggedIn));


        /// <summary>
        /// Dispose the view releasing all classes kept alive by an event
        /// </summary>
        public override void Dispose()
        {
            _accountStore.CurrentUserChanged -= OnCurrentUserChanged;
            base.Dispose();
        }
    }
}
