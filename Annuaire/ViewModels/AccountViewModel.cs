﻿using System.Windows.Input;

using DirectoryUI.Commands;
using DirectoryUI.Services;

using DirectoryUI.Stores;

namespace DirectoryUI.ViewModels
{
    internal class AccountViewModel : ViewModelBase
    {
        private readonly AccountStore _accountStore;

        public string Username => _accountStore.CurrentUser?.Username;

        public ICommand NavigateHomeCommand { get; }

        public AccountViewModel(AccountStore accountStore, INavigationService homeNavigationService)
        {
            _accountStore = accountStore;

            NavigateHomeCommand = new NavigateCommand(homeNavigationService);

            _accountStore.CurrentUserChanged += OnCurrentUserChanged;
        }

        private void OnCurrentUserChanged() => OnPropertyChanged(nameof(Username));


        /// <summary>
        /// Dispose the view releasing all classes kept alive by an event
        /// </summary>
        public override void Dispose()
        {
            _accountStore.CurrentUserChanged -= OnCurrentUserChanged;
            base.Dispose();
        }
    }
}
