﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

using DirectoryUI.Commands;
using DirectoryUI.Services;
using DirectoryUI.Stores;
using DirectoryUI.ViewModels.Objects;

using models.Entities;

namespace DirectoryUI.ViewModels.EmployeeViewModels
{
    internal class UpdateEmployeeViewModel : ViewModelBase
    {
        public override string Header => "Update new Employee";


        private readonly EmployeeStore _employeeStore;
        private readonly DepartmentStore _departmentStore;
        private readonly INavigationService _closeNavigationService;

        private readonly EmployeeViewModel _employee;
        public EmployeeViewModel Employee => _employee;

        private readonly List<Department> _departments;
        public List<Department> Departments => _departments;

        public ICommand SubmitCommand { get; }
        public ICommand CancelCommand { get; }

        public UpdateEmployeeViewModel(INavigationService closeNavigationService, EmployeeStore employeeStore, DepartmentStore departmentStore)
        {

            _closeNavigationService = closeNavigationService;
            _employeeStore = employeeStore;
            _departmentStore = departmentStore;
            LoadUpdateViewModel();

            // If no entry was selected
            if (_employeeStore.GetSelected() == null)
            {
                _closeNavigationService.Navigate();
                return;
            }
            _employee = new EmployeeViewModel(_employeeStore.GetSelected().ConvertToEmployee());


            _departments = _departmentStore.Departments.ToList();
            SubmitCommand = new RelayCommand(Update);
            CancelCommand = new NavigateCommand(closeNavigationService);
        }

        public async void LoadUpdateViewModel()
        {
            await _departmentStore.Load();
        }

        private void Update()
        {
            Employee employeeToUpdate = Employee.ConvertToEmployee();
            string message = _employeeStore.IsValid(employeeToUpdate);
            if (string.IsNullOrWhiteSpace(message))
            {
                _employeeStore.Update(Employee.ConvertToEmployee());
                _closeNavigationService.Navigate();
            }
            else
                MessageBox.Show(message);

        }
    }
}
