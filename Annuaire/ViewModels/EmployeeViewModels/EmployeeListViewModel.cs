﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;

using DirectoryUI.Commands;
using DirectoryUI.Services;
using DirectoryUI.Stores;
using DirectoryUI.ViewModels.Objects;

using models.Entities;

namespace DirectoryUI.ViewModels.EmployeeViewModels
{
    internal class EmployeeListViewModel : ViewModelBase
    {
        public override string Header => "Employees";

        public ICommand ResetFilterCommand { get; set; }

        #region Admin

        public bool IsLoggedIn => _accountStore.IsLoggedIn;
        public ICommand AddEmployeeNavigationCommand { get; set; }
        public ICommand DeleteEmployeeCommand { get; set; }
        public ICommand UpdateEmployeeNavigationCommand { get; set; }

        private EmployeeViewModel selectedEmployee;
        public EmployeeViewModel SelectedEmployee
        {
            get => selectedEmployee;
            set
            {
                if (IsLoggedIn)
                {
                    selectedEmployee = value;
                    OnPropertyChanged(nameof(SelectedEmployee));
                    _employeeStore.AddToSelection(selectedEmployee);
                }
            }
        }

        private void OnCurrentUserChanged() => OnPropertyChanged(nameof(IsLoggedIn));

        private void OnEmployeeCreated(Employee employee)
        {
            _employees.Add(new EmployeeViewModel(employee));
            LoadViewModel();
        }

        private void OnEmployeeUpdated(Employee employee)
        {
            _employees[_employees.IndexOf(SelectedEmployee)] = new EmployeeViewModel(employee);
            LoadViewModel();
        }

        private void OnEmployeeDeleted(Employee employee) => _employees.Remove(SelectedEmployee);

        #endregion

        #region Stores

        private readonly EmployeeStore _employeeStore;
        private readonly DepartmentStore _departmentStore;
        private readonly SiteStore _siteStore;
        private readonly AccountStore _accountStore;

        #endregion

        #region Lists

        private List<Department> _departments;
        public List<Department> Departments 
        { get 
            { 
                if (_siteFilter != Guid.Empty)
                    return _departments.Where(d => d.IdSite == _siteFilter).ToList();
                return _departments;
            }
            set
            {
                _departments = value;
                OnPropertyChanged(nameof(Departments));
            }
        }

        private List<Site> _sites;
        public List<Site> Sites => _sites;

        private readonly ObservableCollection<EmployeeViewModel> _employees  = new();
        public IEnumerable<EmployeeViewModel> Employees => _employees; // Clone of the ObservableCollection for better encapsulation


        #endregion

        #region Filter

        private CollectionViewSource filterableEmployeeSource;
        public ICollectionView FilterableEmployees
        {
            get
            {
                return filterableEmployeeSource.View;
            }
        }


        private string _employeeFilter = string.Empty;
        public string EmployeeFilter
        {
            get
            {
                return _employeeFilter;
            }
            set
            {
                _employeeFilter = value;
                OnPropertyChanged(nameof(EmployeeFilter));
                filterableEmployeeSource.View.Refresh();
            }
        }

        private Guid _departmentFilter;
        public Guid DepartmentFilter
        {
            get
            {
                return _departmentFilter;
            }
            set
            {
                _departmentFilter = value;
                OnPropertyChanged(nameof(DepartmentFilter));
                filterableEmployeeSource.View.Refresh();
            }
        }

        private Guid _siteFilter;
        public Guid SiteFilter
        {
            get
            {
                return _siteFilter;
            }
            set
            {
                _siteFilter = value;
                OnPropertyChanged(nameof(SiteFilter));
                OnPropertyChanged(nameof(Departments));
                filterableEmployeeSource.View.Refresh();
            }
        }

        /// <summary>
        /// Handle the filter Event on change on Employee, Department or Site filter
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="e"></param>
        private void FilterHandler(object obj, FilterEventArgs e)
        {
            EmployeeViewModel employeeViewModel = e.Item as EmployeeViewModel;
            e.Accepted = false;

            // Early return if filter is not empty and the condition is not fullfiled
            if (!SiteFilter.Equals(Guid.Empty))
                if (!employeeViewModel.Department.IdSite.Equals(SiteFilter))
                    return;

            if (!DepartmentFilter.Equals(Guid.Empty))
                if (!employeeViewModel.IdDepartment.Equals(DepartmentFilter))
                    return;
            
            if (!string.IsNullOrWhiteSpace(EmployeeFilter.Trim()))
                if (!employeeViewModel.Name.ToUpper().Contains(EmployeeFilter.ToUpper())
                    && ! employeeViewModel.FamilyName.ToUpper().Contains(EmployeeFilter.ToUpper()))
                        return;
            
            // If all previous tests pass the entry is valid
            e.Accepted = true;
        }

        private void resetFilters()
        {
            EmployeeFilter = string.Empty;
            SiteFilter = Guid.Empty;
            DepartmentFilter = Guid.Empty;
        }


        #endregion

        public EmployeeListViewModel(EmployeeStore employeeStore, AccountStore accountStore, INavigationService addEmployeeNavigationService, INavigationService updateEmployeeNavigationService, DepartmentStore departmentStore, SiteStore siteStore)
        {
            // DI Store
            _employeeStore = employeeStore;
            _departmentStore = departmentStore;
            _siteStore = siteStore;
            _accountStore = accountStore;
            _employeeStore.AddToSelection(null);
            // Loading asynchronously stores and lists
            LoadViewModel();

            // Admin event handling
            _employeeStore.EmployeeCreated += OnEmployeeCreated;
            _employeeStore.EmployeeUpdated += OnEmployeeUpdated;
            _employeeStore.EmployeeDeleted += OnEmployeeDeleted;
            _accountStore.CurrentUserChanged += OnCurrentUserChanged;

            // Binding Admin Command to perform CRUD on employees
            AddEmployeeNavigationCommand = new NavigateCommand(addEmployeeNavigationService);
            UpdateEmployeeNavigationCommand = new NavigateCommand(updateEmployeeNavigationService);
            DeleteEmployeeCommand = new RelayCommand(() => _employeeStore.Delete());
            ResetFilterCommand = new RelayCommand(() => resetFilters());

            // Handling filtering
            filterableEmployeeSource = new() { Source = _employees };
            filterableEmployeeSource.Filter += FilterHandler;
        }
        
        /// <summary>
        /// Loading asynchronously stores and lists 
        /// </summary>
        public async void LoadViewModel()
        {
            await _departmentStore.Load();
            await _siteStore.Load();
            await _employeeStore.Load();

            _employees.Clear();
            foreach (Employee employee in _employeeStore.Employees)
            {
                EmployeeViewModel employeeViewModel = new(employee);
                employeeViewModel.Department = _departmentStore.GetDepartmentFromId(employee.IdDepartment);
                employeeViewModel.Site = _siteStore.GetSiteFromId(employeeViewModel.Department.IdSite);

                _employees.Add(employeeViewModel);
            }
            _employees.OrderBy(e => e.Site).ThenBy(e => e.Department).ThenBy(e => e.FamilyName).ThenBy(e => e.Name);
            
            _departments = _departmentStore.Departments.ToList();
            _sites = _siteStore.Sites.ToList();
        }


        /// <summary>
        /// Dispose the view releasing all classes kept alive by an event
        /// </summary>
        public override void Dispose()
        {
            _employeeStore.EmployeeCreated -= OnEmployeeCreated;
            _employeeStore.EmployeeUpdated -= OnEmployeeUpdated;
            _employeeStore.EmployeeDeleted -= OnEmployeeDeleted;
            _accountStore.CurrentUserChanged -= OnCurrentUserChanged;
            base.Dispose();
        }
    }
}
