﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

using DirectoryUI.Commands;
using DirectoryUI.Services;
using DirectoryUI.Stores;
using DirectoryUI.ViewModels.Objects;

using models.Entities;

namespace DirectoryUI.ViewModels.EmployeeViewModels
{
    internal class AddEmployeeViewModel : ViewModelBase
    {
        public override string Header => "Add new Employee";


        private readonly EmployeeStore _employeeStore;
        private readonly DepartmentStore _departmentStore;
        private readonly INavigationService _closeNavigationService;

        private readonly List<Department> _departments;
        public List<Department> Departments => _departments;

        private readonly EmployeeViewModel _employee = new();
        public EmployeeViewModel Employee => _employee;

        public ICommand SubmitCommand { get; }
        public ICommand CancelCommand { get; }

        public AddEmployeeViewModel(INavigationService closeNavigationService, EmployeeStore employeeStore, DepartmentStore departmentStore)
        {

            _closeNavigationService = closeNavigationService;
            _employeeStore = employeeStore;
            _departmentStore = departmentStore;

            LoadAddViewModel();
            _departments = _departmentStore.Departments.ToList();
            SubmitCommand = new AsyncRelayCommand(Add);
            CancelCommand = new NavigateCommand(closeNavigationService);
        }

        public async void LoadAddViewModel()
        {
            await _departmentStore.Load();
        }

        private async Task Add()
        {
            Employee employeeToAdd = Employee.ConvertToEmployee();
            string message = _employeeStore.IsValid(employeeToAdd);
            if (string.IsNullOrWhiteSpace(message))
            {
                await _employeeStore.Add(employeeToAdd);
                _closeNavigationService.Navigate();
            }
            else
                MessageBox.Show(message);

        }
    }
}
