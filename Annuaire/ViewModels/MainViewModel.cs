﻿using System.Windows.Input;

using DirectoryUI.Commands;
using DirectoryUI.Stores;

namespace DirectoryUI.ViewModels
{
    internal class MainViewModel : ViewModelBase
    {
        public ICommand KeyStrokeTrigger { get; set; }
        public ICommand CloseModal { get; set; }

        public string Username => _accountStore.CurrentUser?.Username;
        public bool IsLoggedIn => _accountStore.IsLoggedIn;
        public bool ModalIsOpen => _navigationStore.ModalIsOpen;

        public ViewModelBase CurrentViewModel => _navigationStore.CurrentViewModel;
        public ViewModelBase CurrentModalViewModel => _navigationStore.CurrentModalViewModel;

        private readonly AccountStore _accountStore;
        private readonly NavigationStore _navigationStore;


        public MainViewModel(NavigationStore navigationStore,
            AccountStore accountStore, 
            AdminTriggerCommand adminTriggerCommand)
        {
            _navigationStore = navigationStore;
            _accountStore = accountStore;

            // Event to change ViewModel
            _navigationStore.CurrentViewModelChanged += navigationStore_CurrentViewModelChanged;
            
            // Event to Close/Load Modal
            _navigationStore.CurrentModalViewModelChanged += navigationStore_CurrentModalViewModelChanged;
            
            // Event to toggle Admin/Viewer view
            _accountStore.CurrentUserChanged += OnCurrentUserChanged;

            KeyStrokeTrigger = adminTriggerCommand;
            CloseModal = new RelayCommand(()=>_navigationStore.Close());
        }

        #region Events

        private void navigationStore_CurrentViewModelChanged() => OnPropertyChanged(nameof(CurrentViewModel));  
        private void OnCurrentUserChanged() => OnPropertyChanged(nameof(IsLoggedIn));
        private void navigationStore_CurrentModalViewModelChanged()
        {
            OnPropertyChanged(nameof(CurrentModalViewModel));
            OnPropertyChanged(nameof(ModalIsOpen));
        }
        
        #endregion

        /// <summary>
        /// Dispose the view releasing all classes kept alive by an event
        /// </summary>
        public override void Dispose()
        {
            _navigationStore.CurrentViewModelChanged -= navigationStore_CurrentViewModelChanged;
            _navigationStore.CurrentModalViewModelChanged -= navigationStore_CurrentModalViewModelChanged;
            base.Dispose();
        }
    }
}
