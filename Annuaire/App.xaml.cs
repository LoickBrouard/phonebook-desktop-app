﻿using System;
using System.Windows;

using DirectoryUI.Commands;
using DirectoryUI.Services;
using DirectoryUI.Stores;

using DirectoryUI.ViewModels;

using Infrastructure;

using Microsoft.Extensions.DependencyInjection;

using UseCases;

namespace DirectoryUI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        protected override void OnStartup(StartupEventArgs e)
        {
            IServiceProvider services = CreateServiceProvider();

            INavigationService initialNavigationService = services.GetRequiredService<INavigationService>();
            
            initialNavigationService.Navigate();

            MainWindow = services.GetRequiredService<MainWindow>();

            MainWindow.Show();

            base.OnStartup(e);
        }

        /// <summary>
        /// DI all providers are in separated files ending with ServiceProvider in matching folder
        /// E.g : StoreServices -> StoreServiceProvider in Stores folder
        /// </summary>
        /// <returns></returns>
        private IServiceProvider CreateServiceProvider()
        {
            return new ServiceCollection()
                .AddViewModelServices()
                .AddInfrastructureServices()
                .AddEntityServices()
                .AddCommandServices()
                .AddNavigationServices()
                .AddStoreServices()
                .BuildServiceProvider();
        }
    }
}