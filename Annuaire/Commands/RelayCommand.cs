﻿using System;

namespace DirectoryUI.Commands
{
    internal class RelayCommand : CommandBase
    {
        private readonly Action _callback;

        public RelayCommand(Action callback) : base()
        {
            _callback = callback;
        }

        public override void Execute(object parameter)
        {
            _callback();
        }
    }
}
