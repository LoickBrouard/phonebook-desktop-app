﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DirectoryUI.Commands
{
    internal abstract class AsyncCommandBase : ICommand
    {

        private bool _isExecuting;
        public bool IsExecuting
        {
            get
            {
                return _isExecuting;
            }
            set
            {
                _isExecuting = value;
                CanExecuteChanged?.Invoke(this, new EventArgs());
            }
        }

        public event EventHandler CanExecuteChanged;

        public AsyncCommandBase()
        {
        }

        public bool CanExecute(object parameter)
        {
            return !IsExecuting;
        }

        public async void Execute(object parameter)
        {
            IsExecuting = true;

            await ExecuteAsync(parameter);

            IsExecuting = false;
        }

        protected abstract Task ExecuteAsync(object parameter);
    }
}
