﻿using System;

using DirectoryUI.Services;
using DirectoryUI.Stores;
using DirectoryUI.ViewModels;

using Microsoft.Extensions.DependencyInjection;

namespace DirectoryUI.Commands
{
    internal static class CommandServiceProvider
    {
        public static IServiceCollection AddCommandServices(this IServiceCollection services)
        {
            return services
                .AddTransient(s => new AdminTriggerCommand(CreateLoginModalNavigationService(s)));
        }

        private static INavigationService CreateLoginModalNavigationService(IServiceProvider serviceProvider)
        {
            return new ModalNavigationService<LoginFormViewModel>(
                serviceProvider.GetRequiredService<NavigationStore>(),
                () => serviceProvider.GetRequiredService<LoginFormViewModel>());
        }
    }
}
