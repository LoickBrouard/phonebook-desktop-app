﻿using System.Windows.Input;
using System.Windows;
using DirectoryUI.Services;

namespace DirectoryUI.Commands
{
    public class AdminTriggerCommand : CommandBase
    {
        private readonly INavigationService _navigationService;

        public AdminTriggerCommand(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public override bool CanExecute(object parameter)
        {
            return Keyboard.IsKeyDown(Key.A)
                && Keyboard.IsKeyDown(Key.D)
                && Keyboard.IsKeyDown(Key.M)
                && Keyboard.IsKeyDown(Key.I)
                && Keyboard.IsKeyDown(Key.N)
                && base.CanExecute(parameter);
        }
        public override void Execute(object parameter)
        {
            MessageBox.Show("Combinaison secrete !", "ADMINADMINADMINADMIN", MessageBoxButton.OK);
            NavigateCommand openLoginModal = new (_navigationService);
            openLoginModal.Execute(parameter);
        }
    }
}
