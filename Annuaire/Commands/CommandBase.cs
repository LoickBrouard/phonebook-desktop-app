﻿using System;
using System.Windows.Input;

namespace DirectoryUI.Commands
{
    public abstract class CommandBase : ICommand
    {
        public event EventHandler CanExecuteChanged;

        /// <summary>
        /// Method defining if Execute method can be executed when a command is triggered (return true by default)
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public virtual bool CanExecute(object parameter)
        {
            return true;
        }

        /// <summary>
        /// Method needed to be implemented by children classes.
        /// <br/>
        /// Define the behaviour when a command is triggered
        /// </summary>
        /// <param name="parameter"></param>
        public abstract void Execute(object parameter);

        /// <summary>
        /// Method used to trigger manually the event CanExecuteChanged
        /// <br/>
        /// To be call when CanExecute is overrided
        /// </summary>
        protected void OnCanExecutedChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
