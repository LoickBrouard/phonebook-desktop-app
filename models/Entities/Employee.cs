﻿using Utilities;

namespace models.Entities
{
    public class Employee
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedDate { get;}
        public string FamilyName { get; set; }
        public string Phone { get; set; }
        public string? CellPhone { get; set; }
        public string? Email { get; set; }
        public Guid IdDepartment { get; set; }

        public Employee(string name, string familyName, string email, Guid idDepartment, string phone = "", string cellPhone = "")
        {
            Id = new Guid();
            Name = name.ToProperNoun();
            CreatedDate = DateTime.Now;
            FamilyName = familyName.ToProperNoun();
            Phone = phone;
            CellPhone = cellPhone;
            Email = email;
            IdDepartment = idDepartment;
        }

        public Employee(Guid id, string name, string familyName, string email, Guid idDepartment, string phone = "", string cellPhone = "")
        {
            Id = id;
            Name = name.ToProperNoun();
            CreatedDate = DateTime.Now;
            FamilyName = familyName.ToProperNoun();
            Phone = phone;
            CellPhone = cellPhone;
            Email = email;
            IdDepartment = idDepartment;
        }

        public Employee()
        {
            Id = Guid.NewGuid();
        }

        public override string ToString() => $"{Name} {FamilyName}";

        public override bool Equals(object? obj)
        {
            if(obj == null) return false;
            if(obj == this) return true;
            if(this.GetType() != obj.GetType()) return false;
            if(this.GetType() == obj.GetType())
            {
                Employee employee = (Employee)obj;
                return employee.Id == Id;
            }
            return false;
        }

        public override int GetHashCode() => base.GetHashCode();
    }
}
