﻿namespace models.Entities
{
    public class Department
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public Guid IdSite { get; set; }

        public Department()
        {
            Id = Guid.NewGuid();
        }

        public Department(string name, string type, Guid idSite)
        {
            Id = new Guid();
            Name = name;
            Type = type;
            IdSite = idSite;
        }

        public Department(Guid id, string name, string type, Guid idSite)
        {
            Id = id;
            Name = name;
            Type = type;
            IdSite = idSite;
        }

        public override string ToString() => $"{Name} ({Type})";

    }
}
