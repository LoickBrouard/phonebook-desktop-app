﻿namespace models.Entities
{
    public class User
    {
        public Guid Id { get;}
        public string Username { get; }
        public string Password { get; }

        public User(string username, string password)
        {
            Id = new Guid();
            Username = username;
            Password = password;
        }
        
        public User(Guid id, string username, string password)
        {
            Id = id;
            Username = username;
            Password = password;
        }
    }
}
