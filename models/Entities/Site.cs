﻿namespace models.Entities
{
    public class Site
    {
        public Guid Id { get; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }

        public Site()
        {
            Id = Guid.NewGuid();    
        }

        public Site(string name, string address, string zipCode, string city)
        {
            Id = new Guid();
            Name = name;
            Address = address;
            ZipCode = zipCode;
            City = city;
        }

        public Site(Guid id, string name, string address, string zipCode, string city)
        {
            Id = id;
            Name = name;
            Address = address;
            ZipCode = zipCode;
            City = city;
        }
    }
}
