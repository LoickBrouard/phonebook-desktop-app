using System.Collections.Generic;

using models.Entities;

using Moq;

using UseCases;

using Xunit;

namespace UnitTests
{
    public class UsesCasesTests
    {
        [Fact]
        public void DisplayDirectory_GetEmployeesWhenEmployeesPresent_ReturnListOfEmplopyees()
        {
            // Arrange
            IEnumerable<Employee> employeesExpected = new List<Employee>() {
                new Employee("Roger", "Rabbit", "roger.rabbit", new System.Guid()),
                new Employee("Marty", "McFly", "marty.McFly", new System.Guid()),
            };
                    /// Cr�ation du mock de IEmployeeRepository
            var employeeRepositoryMock = new Mock<IEmployeeRepository>();
            employeeRepositoryMock.Setup(m => m.GetAll()).Returns(employeesExpected);
            IEmployeeRepository employeeRepositoryMockInstance = employeeRepositoryMock.Object;
            
            EmployeesService displayDirectory = new(employeeRepositoryMockInstance);

            // Act
            IEnumerable<Employee> employeesGet = displayDirectory.GetAll();

            // Assert
            Assert.Equal(employeesExpected, employeesGet);
        }
    }
}