﻿using models.Entities;

using UseCases.Contracts;

namespace UseCases
{
    public class DemoService
    {
        private readonly IUnitOfWork _unitOfWork;

        public DemoService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async void LoadDemo()
        {
            _unitOfWork.EnsureDeleted();
            _unitOfWork.EnsureCreated();

            List<Site> sites = new()
            {
                new("Paris", "2, Avenue des Champs Elysées", "75000", "Paris"),
                new("Nantes", "5649, Boulevard du Petit Port", "44300", "Nantes"),
                new("Toulouse", "1, Place de la Daurade", "31000", "Toulouse"),
                new("Nice", "Christ's Drive", "CA-609", "Nice"),
                new("Lille", "505 , Avenue. Mathias Delobel", "59000", "Lille")
            };

            foreach (Site site in sites)
                await _unitOfWork.SiteRepository.CreateAsync(site);

            _unitOfWork.SaveChanges();

            Dictionary<string, string> departmentForDatabase = new ()
            {
                { "Acountant", "ACC"},
                { "Human Ressource", "HR"},
                { "Development", "Dev"},
                { "Juridic", "Jur"},
            };

            foreach (KeyValuePair<string, string> entry in departmentForDatabase)
                foreach (Site site in sites)
                    await _unitOfWork.DepartmentRepository.CreateAsync(new Department($"{site.Name[..3]}_{entry.Key}", entry.Key, site.Id));

            _unitOfWork.SaveChanges();

            List<Department> departmentsSaved = _unitOfWork.DepartmentRepository.GetAll().ToList();

            List<string> familyNames = new List<string>()
            {
"SMITH",
"MARTIN",
"JONES",
"BROWN",
"THOMAS",
"JOHNSON",
"WILLIAMS",
"WILSON",
"MILLER",
"TAYLOR",
"DAVIS",
"BERNARD",
"WHITE",
"CLARK",
"HALL",
"ROBERT",
"ANDERSSON",
"RICHARD",
"HENRY",
"THOMPSON",
"MOORE",
"ROUX",
"HILL",
"PETIT",
"ANDERSON",
"SIMON",
"WALKER",
"DUBOIS",
"MICHEL",
"DURAND",
"WRIGHT",
"WOOD",
"SCOTT",
"ALLEN",
"MOREAU",
"ROBINSON",
"ADAMS",
"YOUNG",
"LEWIS",
"DAVID",
"JACKSON",
"KING",
"JOHN",
"GREEN",
"BAKER",
"EVANS",
"CAMPBELL",
"LAMBERT",
"ROY",
"JAMES",
"HANSEN",
"TRYNISKI",
"HARRIS",
"LAURENT",
"ROBERTS",
"GIRARD",
"OLSEN",
"STEWART",
"BERTRAND",
"BLANC",
"GEORGE",
"FOURNIER",
"LEE",
"MOREL",
"VINCENT",
"PARKER",
"COUNTY",
"ROUSSEAU",
"MC",
"TURNER",
"COOK",
"GARNIER",
"GUERIN",
"BELL",
"ANDRE",
"WARD",
"MORRIS",
"EDWARDS",
"MITCHELL",
"JONSSON",
"FAURE",
"JOHANSSON",
"MERCIER",
"WATSON",
"BONNET",
"PEDERSEN",
"LEROY",
"MORGAN",
"MORIN",
"ANDERSDR",
"GAUTIER",
"COOPER",
"GRAY",
"PERRIN",
"DUPONT",
"JENSEN",
"ROGERS",
"PHILLIPS",
"MARCHAND"
            };

            List<string> names = new List<string>() { "Etienne", "Jean", "Paul", "Kevin", "Franck", "Antoine", "Georges", "Marc", "Piotre", "Maxime", "Laurent", "Pascal", "Jean-Marie", "François", "Alexandre", "Jean-Christophe", "Sophie", "Antoinette", "Emilie", "Edith", "Laurianee", "Lilith", "Laurence", "Florence", "Christiane", "Rory", "Auriane", "Manon", "Daniel", "Elizabeth", "Myriam", "Thierry", "Clément", "Jean-Marie" };

            var employeesCreationTasks = new List<Task>();

            Random random = new ();
            for (int i = 0; i < 1001; i++)
            {
                string name = names[random.Next(names.Count)];
                string familyName = familyNames[random.Next(familyNames.Count)];
                Site site = sites[random.Next(sites.Count)];
                List<Department> departments = departmentsSaved.Where(d => d.IdSite == site.Id).ToList();
                Guid departmentId = departments[random.Next(departments.Count)].Id;
                string email = $"{name}.{familyName}@{site.Name}.net";

                employeesCreationTasks.Add(_unitOfWork.EmployeeRepository.CreateAsync(new Employee(name, familyName, email, departmentId, random.NextInt64().ToString(), random.NextInt64().ToString())));
            }

            await Task.WhenAll(employeesCreationTasks);
            _unitOfWork.SaveChanges();
        }
    }
}
