﻿
using models.Entities;

using UseCases.Contracts;

namespace UseCases
{
    public class SiteService
    {
        private readonly ISiteRepository _siteRepository;
        private readonly IDepartmentRepository _departmentRepository;

        public SiteService(ISiteRepository siteRepository, IDepartmentRepository departmentRepository)
        {
            _siteRepository = siteRepository;
            _departmentRepository = departmentRepository;
        }

        public Task AddAsync(Site site) => _siteRepository.CreateAsync(site);

        public Site Update(Site site) => _siteRepository.Update(site);

        public void Delete(Site site) => _siteRepository.Delete(site);

        public IEnumerable<Site> GetSites() => _siteRepository.GetAll();
        
        public Task<List<Site>> GetSitesAsync() => _siteRepository.GetAllAsync();

        public Site GetSiteFromId(Guid id) =>_siteRepository.GetWithConditions(site => site.Id == id).First();
        
        public Site? GetSiteFromName(string name) =>_siteRepository.GetWithConditions(site => site.Name == name).FirstOrDefault();

        public bool IsContainingDepartment(Guid siteId) => _departmentRepository.GetWithConditions(department => department.IdSite == siteId).Any();
    }
}
