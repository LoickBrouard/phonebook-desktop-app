﻿
using models.Entities;

namespace UseCases
{
    public class EmployeesService
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeesService(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public Task AddAsync(Employee employee)
        {
            return _employeeRepository.CreateAsync(employee);
        }
        
        public Employee Update(Employee employee)
        {
            return _employeeRepository.Update(employee);
        }

        public void Delete(Employee employee)
        {
            _employeeRepository.Delete(employee);
        }

        public IEnumerable<Employee> GetAll()
        {
            return _employeeRepository.GetAll();
        }
        
        public Task<List<Employee>> GetAllAsync()
        {
            return _employeeRepository.GetAllAsync();
        }
    }
}
