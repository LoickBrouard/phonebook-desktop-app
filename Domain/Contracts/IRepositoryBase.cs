﻿using System.Linq.Expressions;

namespace UseCases
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        public IEnumerable<TEntity> GetAll();
        public Task<List<TEntity>> GetAllAsync();
        public IEnumerable<TEntity> GetWithConditions(Expression<Func<TEntity, bool>> expression);
        public void Delete(TEntity entity);
        public TEntity Update(TEntity entity);
        public Task<TEntity> CreateAsync(TEntity entity);
        public void SaveChanges();

    }
}
