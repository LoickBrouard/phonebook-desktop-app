﻿
using models.Entities;

namespace UseCases.Contracts
{
    public interface IDepartmentRepository : IRepositoryBase<Department>
    {
    }
}
