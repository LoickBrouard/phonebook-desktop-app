﻿namespace UseCases.Contracts
{
    public interface IUnitOfWork
    {
        IEmployeeRepository EmployeeRepository { get; }
        IDepartmentRepository DepartmentRepository { get; }
        ISiteRepository SiteRepository { get; }

        void SaveChanges();
        void EnsureCreated();
        void EnsureDeleted();
    }
}
