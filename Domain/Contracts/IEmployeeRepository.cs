﻿
using models.Entities;

namespace UseCases
{
    public interface IEmployeeRepository : IRepositoryBase<Employee>
    { 
    }
}
