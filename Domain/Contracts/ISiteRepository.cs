﻿
using models.Entities;

namespace UseCases.Contracts
{
    public interface ISiteRepository : IRepositoryBase<Site>
    { 
    }
}
