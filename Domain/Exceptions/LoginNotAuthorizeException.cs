﻿namespace UseCases.Exceptions
{
    internal class LoginNotAuthorizeException : ApplicationException
    {
        public LoginNotAuthorizeException() : base(){}
        public LoginNotAuthorizeException(string message) : base(message){}
        public LoginNotAuthorizeException(string message, Exception innerException) : base(message, innerException){}
    }
}
