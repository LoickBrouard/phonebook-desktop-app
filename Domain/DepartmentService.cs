﻿
using models.Entities;

using UseCases.Contracts;

namespace UseCases
{
    public class DepartmentService
    {
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IEmployeeRepository _employeeRepository;

        public DepartmentService(IDepartmentRepository departmentRepository, IEmployeeRepository employeeRepository)
        {
            _departmentRepository = departmentRepository;
            _employeeRepository = employeeRepository;
        }

        public Task AddAsync(Department department) => _departmentRepository.CreateAsync(department);

        public Department Update(Department department) => _departmentRepository.Update(department);

        public void Delete(Department department) => _departmentRepository.Delete(department);

        public IEnumerable<Department> GetDepartments() => _departmentRepository.GetAll();

        public Task<List<Department>> GetDepartmentsAsync() => _departmentRepository.GetAllAsync();

        public Department GetDepartmentFromId(Guid guid) => _departmentRepository.GetWithConditions(d => d.Id == guid).First();

        public Department? GetDepartmentFromName(string name) => _departmentRepository.GetWithConditions(d => d.Name == name).FirstOrDefault();

        public bool IsContainingEmployee(Guid departmentId) => _employeeRepository.GetWithConditions(e => e.IdDepartment == departmentId).Any();
    }
}
